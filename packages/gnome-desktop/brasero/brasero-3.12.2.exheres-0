# Copyright 2009 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] gsettings gtk-icon-cache freedesktop-desktop freedesktop-mime

SUMMARY="An application to burn CD/DVD for the GNOME Desktop"
HOMEPAGE="http://projects.gnome.org/brasero"

LICENCES="GPL-2"
SLOT="3"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="gobject-introspection gtk-doc nautilus playlist tracker
    ( linguas:
        af ar as ast be bg bn bn_IN br bs ca ca@valencia cs da de dz el en_CA en_GB en@shaw eo es et
        eu fa fi fr fur ga gd gl gu he hi hu id is it ja kk km kn ko ku lt lv mai mk ml mr ms my nb
        nds ne nl nn oc or pa pl pt pt_BR ro ru sk sl sr sr@latin sv ta te tg th tr ug uk vi zh_CN
        zh_HK zh_TW
    )
"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.50]
        gnome-desktop/yelp-tools
        sys-devel/gettext
        virtual/pkg-config[>=0.20]
        gtk-doc? ( dev-doc/gtk-doc[>=1.12] )
    build+run:
        dev-libs/glib:2[>=2.29.14]
        dev-libs/libxml2:2.0[>=2.6.0]
        media-libs/gstreamer:1.0[>=0.11.92]
        media-libs/libcanberra[>=0.1][providers:gtk3]
        media-plugins/gst-plugins-base:1.0[>=0.11.92]
        x11-libs/libSM
        x11-libs/libICE
        x11-libs/gtk+:3[>=3.0.0][gobject-introspection?]
        x11-libs/libnotify[>=0.6.1]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.30.0] )
        nautilus? ( gnome-desktop/nautilus[>=2.91.90] )
        playlist? ( gnome-desktop/totem-pl-parser[>=2.29.1] )
        tracker? ( app-pim/tracker:= )
    run:
        app-cdr/dvd+rw-tools [[ description = [ needed to burn files larger than 2 GiB ] ]]
    test:
        app-text/docbook-xml-dtd:4.3
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    '--disable-libburnia'
    '--enable-preview'
    '--disable-caches'
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'gobject-introspection introspection'
    'gtk-doc'
    playlist
    nautilus
    'tracker search'
)

src_prepare() {
    default

    edo sed -e "s/pkg-config/$(exhost --tool-prefix)&/" -i configure
}

pkg_postinst() {
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
}

pkg_postrm() {
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
}

