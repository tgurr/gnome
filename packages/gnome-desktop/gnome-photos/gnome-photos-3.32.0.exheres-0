# Copyright 2013 Marvin Schmidt <marv@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] gsettings gtk-icon-cache

SUMMARY="Access, organize and share your photos on GNOME"
HOMEPAGE="https://wiki.gnome.org/Apps/Photos"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( linguas: ar as bg ca ca@valencia cs da de el eo es et eu fa fi fr ga gl he hu id it ja kn ko
               lt lv ml nb nl pa pl pt pt_BR ru sk sl sr sr@latin te tg tr uk zh_CN zh_HK zh_TW )
"

DEPENDENCIES="
    build:
        dev-libs/libxml2:2.0
        dev-util/desktop-file-utils
        dev-util/intltool[>=0.50.1]
        gnome-desktop/yelp-tools
        sys-devel/gettext[>=0.19.8]
        virtual/pkg-config
    build+run:
        app-pim/tracker:=
        app-pim/tracker-miners:2.0
        dev-libs/libdazzle:1.0[>=3.26.0]
        dev-libs/gexiv2[>=0.10.8]
        dev-libs/glib:2[>=2.57.2]
        gnome-desktop/gobject-introspection:1
        gnome-desktop/gsettings-desktop-schemas
        gnome-desktop/geocode-glib:1.0
        gnome-desktop/gnome-desktop:3.0
        gnome-desktop/gnome-online-accounts[>=3.8.0]
        gnome-desktop/grilo:0.3[>=0.3.5]
        gnome-desktop/libgdata[>=0.15.2][online-accounts]
        media-libs/babl
        media-libs/gegl:0.4[>=0.4.0][jpeg2000][raw]
        media-libs/gfbgraph:0.2[>=0.2.1]
        media-libs/libpng:1.6
        x11-libs/cairo[>=1.14.0]
        x11-libs/gdk-pixbuf:2.0[>=2.36.8]
        x11-libs/gtk+:3[>=3.22.16]
        providers:ijg-jpeg? ( media-libs/jpeg:=[>=8] )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
    suggestion:
        gnome-desktop/gnome-online-miners [[ description = [ For crawling facebook photos ] ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-nls
    --disable-Werror
    --disable-dogtail
    --disable-static
)

src_prepare() {
    default
    edo intltoolize --force --automake
    edo autopoint --force
}

pkg_postinst() {
    gtk-icon-cache_pkg_postinst
    gsettings_pkg_postinst
}

pkg_postrm() {
    gtk-icon-cache_pkg_postrm
    gsettings_pkg_postrm
}

