# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]

SUMMARY="C++ bindings for GTK+"
HOMEPAGE="http://www.gtkmm.org/"

LICENCES="LGPL-2.1"
SLOT="3"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    disable-deprecated [[ description = [ Omit deprecated API from the library ] ]]
    doc
    wayland
    X
"

UPSTREAM_DOCUMENTATION="
    http://library.gnome.org/devel/gtkmm/stable/ [[
        lang = en
        description = [ Reference manual ]
    ]]
    http://library.gnome.org/devel/gtkmm/stable/deprecated.html [[
        lang = en
        description = [ Deprecated API list ]
    ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.20]
        doc? ( dev-lang/perl:*[>=5.6.1] )
    build+run:
        dev-cpp/cairomm:1.0[>=1.12.0]
        dev-cpp/libsigc++:2[>=2.0.0]
        gnome-bindings/atkmm:1.6[>=2.24.2]
        gnome-bindings/glibmm:2.4[>=2.54.0][disable-deprecated=][doc?]
        gnome-bindings/pangomm:1.4[>=2.38.2]
        x11-libs/gdk-pixbuf:2.0[>=2.35.5]
        x11-libs/gtk+:3[>=3.24.0][X?][wayland?]
"

# Tests require X
RESTRICT="test"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    # currently not needed
    --disable-broadway-backend
    --disable-quartz-backend
    --disable-win32-backend
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    '!disable-deprecated deprecated-api'
    'doc documentation'
    'X x11-backend'
    'wayland wayland-backend'
)

