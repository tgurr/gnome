# Copyright 2014 Marc-Antoine Perennou <Marc-Antoine@Perennou.com>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] gsettings gtk-icon-cache freedesktop-desktop meson

SUMMARY="A simple GNOME 3 maps application"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( linguas: af an ar as ca ca@valencia cs da de el en_GB eo es et eu fa fi fr fur ga gl he hu id
               it ja kn ko lt lv ml nb nl pa pl pt pt_BR ru sk sl sr sr@latin sv te tg tr uk zh_CN
               zh_HK zh_TW )
"

DEPENDENCIES="
    build:
        dev-lang/perl:*[>=5.8.1]
        dev-util/intltool[>=0.40.0]
        sys-devel/gettext
        virtual/pkg-config[>=0.22]
    build+run:
        base/libgee:0.8[>=0.16.0]
        dev-libs/glib:2[>=2.44]
        dev-libs/libxml2:2.0
        gnome-bindings/gjs:1[>=1.50]
        gnome-desktop/geocode-glib:1.0[>=3.15.2][gobject-introspection]
        gnome-desktop/gobject-introspection:1[>=0.10.1]
        gps/geoclue:2.0[>=0.12.99][gobject-introspection]
        net-im/folks[>=0.10.0]
        net-libs/rest[>=0.7.90][gobject-introspection]
        x11-libs/gtk+:3[>=3.22.0][gobject-introspection]
        x11-libs/libchamplain:0.12[>=0.12.14][gobject-introspection]
    run:
        dev-libs/libsecret:1[gobject-introspection]
        gnome-desktop/libgweather:3.0[gobject-introspection(+)]
        gnome-desktop/libsoup:2.4[gobject-introspection]
        media-libs/gfbgraph:0.2[gobject-introspection]
        x11-libs/clutter:1[gobject-introspection]
        x11-libs/clutter-gtk:1.0[gobject-introspection]
        x11-libs/cogl:1.0[gobject-introspection][?X]
        x11-libs/gdk-pixbuf:2.0[gobject-introspection]
    suggestion:
        gnome-desktop/gnome-clocks [[
            description = [ Add a selected place to GNOME Clocks ]
        ]]
        gnome-desktop/gnome-online-accounts[gobject-introspection] [[
            description = [ Foursquare and Facebook integration using GOA ]
        ]]
        gnome-desktop/gnome-weather [[
            description = [ Show weather forecast for a selected place using GNOME Weather ]
        ]]
        net-libs/webkit:4.0[gobject-introspection] [[
            description = [ Access OpenStreetMap account ]
        ]]
"

BUGS_TO="keruspe@exherbo.org"

pkg_postinst() {
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
    freedesktop-desktop_pkg_postinst
}

pkg_postrm() {
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
    freedesktop-desktop_pkg_postrm
}

