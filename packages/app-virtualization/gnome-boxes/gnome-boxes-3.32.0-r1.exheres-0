# Copyright 2012-2013 Marc-Antoine Perennou <Marc-Antoine@Perennou.com>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require gsettings
require meson
require vala [ vala_dep=true ]
require gtk-icon-cache freedesktop-desktop

SUMMARY="A simple GNOME 3 application to access remote or virtual systems"

LICENCES="LGPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="ovirt"

HOMEPAGE="https://wiki.gnome.org/Boxes"

DEPENDENCIES="
    build:
        gnome-desktop/yelp-tools
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        app-arch/libarchive[>=3.0.0]
        app-pim/tracker:2.0
        dev-libs/glib:2[>=2.44.0]
        dev-libs/gtk-vnc:2.0[>=0.4.4][vapi]
        dev-libs/libosinfo:1.0[>=1.4.0][vapi]
        dev-libs/libsecret:1[vapi]
        dev-libs/libusb:1[>=1.0.9]
        dev-libs/libxml2:2.0[>=2.7.8]
        dev-libs/vte:2.91[>=0.40.2][vapi]
        gnome-desktop/gobject-introspection:1
        gnome-desktop/libgudev[>=165]
        gnome-desktop/libsoup:2.4[>=2.38]
        net-libs/webkit:4.0
        sys-apps/systemd
        sys-apps/util-linux
        virtualization-lib/libvirt-glib:1.0[>=2.0.0][vapi]
        virtualization-ui/spice-gtk:3.0[>=0.32][vapi][usbredir]
        x11-libs/gtk+:3[>=3.22.2]
        ovirt? ( virtualization-lib/govirt[>=0.3.4] )
    run:
        app-virtualization/qemu[>=1.3][spice]
        virtualization-lib/libvirt[kvm]
        x11-misc/shared-mime-info
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Drdp=false
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    ovirt
)

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

